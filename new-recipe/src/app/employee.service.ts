import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class EmployeeService {

 constructor(private http: Http) {
}

saveEmoployee(user: any) {
    const json = JSON.stringify(user);
    const header = new Headers();
    header.append('Content-Type', 'application/json')
    return this.http.post('/api/employee/save', json, {
      headers: header
    })
      .map((data: Response) => data.json());
  }

}
