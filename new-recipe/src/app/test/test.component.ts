import { Component, OnInit } from '@angular/core';
import { HTTPServiceService } from '../httpservice.service';
import { Response } from '@angular/http';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  providers: [HTTPServiceService]
})
export class TestComponent {
  getData: string;
  postData: string;

  items: any[] =[];

  constructor(private httpservice: HTTPServiceService) { }

  ngOnInit() {
  }

  onTestPost(username: string, email: string) {
    this.httpservice.postJson({ username: username, email: email })
      .subscribe(
      data => console.log(data)
      );
  }

  onTestGet() {
    console.log('input test');
    this.httpservice.getJson()
      .subscribe(
      (data: Response) => console.log(data)
      );
  }

  onTestGetData() {
    console.log("load data");
    this.httpservice.onGetData()
      .subscribe(
        data =>{
          const myArray =[];
          for(let key in data){
            myArray.push(data[key]);
          }
          this.items= myArray;
        }
      );
  }


}
