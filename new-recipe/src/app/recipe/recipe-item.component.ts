import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { RecipeController } from '../recipe-controller';

@Component({
  selector: 'app-recipe-item',
  templateUrl: './recipe-item.component.html'
})

export class RecipeItemComponent implements OnInit {

  @Input() recipe: RecipeController;
  recipeId: number;
  @Output() recipeSelected = new EventEmitter<RecipeController>();

  constructor() { }

  ngOnInit() {
  }

  onButtonClicked(recipe: RecipeController) {
    alert("test button clicked!");
    this.recipeSelected.emit(recipe);
  }

}
