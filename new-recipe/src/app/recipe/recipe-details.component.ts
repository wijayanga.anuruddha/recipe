import { Component, OnInit, Input } from '@angular/core';
import { RecipeController } from '../recipe-controller';

@Component({
  selector: 'app-recipe-details',
  templateUrl: './recipe-details.component.html'
})
export class RecipeDetailsComponent implements OnInit {

  @Input() selectedRecipe: RecipeController;

  constructor() { }

  ngOnInit() {
  }
  
}
