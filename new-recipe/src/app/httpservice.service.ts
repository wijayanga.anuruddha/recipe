import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Headers } from '@angular/http';
import { RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Response } from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Rx';

@Injectable()
export class HTTPServiceService {

  constructor(private http: Http) {
  }

  getJson() {
    return this.http.get("/api/rest/load.json")
      .map((response: Response) => response.json());
  }

  postJson(user: any) {
    const json = JSON.stringify(user);
    const header = new Headers();
    header.append('Content-Type', 'application/json')
    // https://recipe-7f720.firebaseio.com/data.json
    return this.http.post('/api/rest/test', json, {
      headers: header
    })
      .map((data: Response) => data.json());
  }

  onGetData() {
    return this.http.get('/api/rest/load.json')
      .map((response: Response) => response.json());
    // .catch(this.handleError);
  }

  // shopping list save
  onPostShoppingList(user: any) {
    const json = JSON.stringify(user);
    console.log(json);
    const header = new Headers();
    let options = new RequestOptions({ headers: header });
    header.append('Content-Type', 'application/json')
    return this.http.post('/api/rest/save', json, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  private handleError(error: any) {
    console.error('An error occurred while accessing service', error);
    return Observable.throw(error);
  }

  //attribute list save
  onPostAttribute(user: any) {
    const json = JSON.stringify(user);
    console.log(json);
    const header = new Headers();
    let options = new RequestOptions({ headers: header });
    header.append('Content-Type', 'application/json')
    return this.http.post('/api/attribute/save', json, options)
      .map((response: Response) => response.json())
      .catch(this.handleError);
  }

  onGetDataEmployee() {
    return this.http.get('/api/employee/load/attribute/emp')
      .map((response: Response) => response.json());
  }

  onGetAttribute() {
    return this.http.get('/api/attribute/load')
      .map((response: Response) => response.json());
  }

  //employee save method 
  onSaveEmoployee(user: any) {
    const json = JSON.stringify(user);
    const header = new Headers();
    header.append('Content-Type', 'application/json')
    return this.http.post('/api/employee/save', json, {
      headers: header
    })
      .map((data: Response) => data.json());
  }

  //load attribute object using attribute id
  onLoadAttribute(id: any) {
   return this.http.get('/api/attribute/load/id')
      .map((response: Response) => response.json());
  }

}
