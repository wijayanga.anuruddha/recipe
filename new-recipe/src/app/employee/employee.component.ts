import { Component, OnInit } from '@angular/core';
import { HTTPServiceService } from '../httpservice.service';
import { EmployeeService } from '../employee.service';
import { AttributeController } from '../attribute-master-controller';
import { Empployee } from '../employee';
import { NgForm } from '@angular/forms';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {MdButtonModule, MdCheckboxModule} from '@angular/material';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html'
})
export class EmployeeComponent implements OnInit {

  items: any[] = [];
  id: any;
  attributeValue: string;
  employeeList: Empployee;
  attribute: AttributeController;

  constructor(private httpservice: HTTPServiceService) { }

  public selectedItem = { name: "" };

  ngOnInit() {
  }

  onLoadData() {
    this.httpservice.onGetDataEmployee()
      .subscribe(
      data => {
        const myArray = [];
        for (let key in data) {
          myArray.push(data[key]);
        }
        this.items = myArray;
      }
      );
  }

  onAddEmployee(form: NgForm) {
    console.log(form.value);

    for (let key in form.value) {

      this.attributeValue = form.value[key];
      this.id = key;

      this.onLoadAttributeUsingId(this.id);

      this.employeeList = new Empployee(this.attributeValue, this.id);

      // this.httpservice.onSaveEmoployee(this.employeeList)
      // .subscribe(
      // data => console.log(data)
      // );

    }
  }

  onLoadAttributeUsingId(id: any) {
     return this.httpservice.onLoadAttribute(id)
      .subscribe(
      data => {
        const myArray = [];
        for (let key in data) {
          myArray.push(data[key]);
        }
        this.items = myArray;
      }
      );
  }

}
