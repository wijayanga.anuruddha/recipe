import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { trigger, state, style, transition, animate } from '@angular/animations';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { RecipeComponent } from './recipe/recipe.component';
import { RecipeItemComponent } from './recipe/recipe-item.component';
import { MenuComponent } from './menu/menu.component';
import { ShoppingListAddComponent } from './shopping-list-add/shopping-list-add.component';
import { RecipeDetailsComponent } from './recipe/recipe-details.component';
import {routing} from "./app.routing";

import { AddService } from './add.service';
import { HTTPServiceService } from './httpservice.service';

import { TestComponent } from './test/test.component';
import { ShoppingListLoadComponent } from './shopping-list-load/shopping-list-load.component';
import { TestNewComponent } from './test-new/test-new.component';
import { AnimationComponent } from './animation/animation.component';
import { TwoWayComponent } from './two-way/two-way.component';
import { AttributeMasterComponent } from './attribute-master/attribute-master.component';
import { EmployeeComponent } from './employee/employee.component';
import { MaterialComponent } from './material/material.component';
import { UserComponent } from './user/user.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    RecipeComponent,
    RecipeItemComponent,
    MenuComponent,
    ShoppingListAddComponent,
    RecipeDetailsComponent,
    TestComponent,
    ShoppingListLoadComponent,
    TestNewComponent,
    AnimationComponent,
    TwoWayComponent,
    AttributeMasterComponent,
    EmployeeComponent,
    MaterialComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing
  ],
  providers: [AddService, HTTPServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
