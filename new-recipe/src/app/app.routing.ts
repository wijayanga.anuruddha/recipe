import { Routes, RouterModule } from "@angular/router";
import { ModuleWithProviders } from "@angular/core";

import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { TestComponent } from './test/test.component';
import { ShoppingListAddComponent } from './shopping-list-add/shopping-list-add.component';
import { ShoppingListLoadComponent } from './shopping-list-load/shopping-list-load.component';
import { AnimationComponent } from './animation/animation.component';
import { TwoWayComponent } from './two-way/two-way.component';
import { AttributeMasterComponent } from './attribute-master/attribute-master.component';
import { EmployeeComponent } from './employee/employee.component';
import { MaterialComponent } from './material/material.component';
import { UserComponent } from './user/user.component';

const APP_ROUTES: Routes = [
    { path: '', component: MainComponent },
    { path: 'add', component: ShoppingListAddComponent },
    { path: 'load', component: ShoppingListLoadComponent },
    { path: 'test', component: TestComponent },
    { path: 'animation', component: AnimationComponent },
    { path: 'binding', component: TwoWayComponent },
    { path: 'master-table', component: AttributeMasterComponent },
    { path: 'employee', component: EmployeeComponent },
    { path: 'material', component: MaterialComponent },
    { path: 'user', component: UserComponent }
];

export const routing = RouterModule.forRoot(APP_ROUTES);