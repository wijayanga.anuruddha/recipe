import { Component, OnInit } from '@angular/core';
import { AddService } from '../add.service';
import { AddShoppingListController } from '../Add-shopping-list-controller';
import { HTTPServiceService } from '../httpservice.service';

@Component({
  selector: 'app-shopping-list-add',
  templateUrl: './shopping-list-add.component.html',
  providers: [AddService, HTTPServiceService]
})

export class ShoppingListAddComponent {

shoppingList: AddShoppingListController;
a: string;

constructor(private httpservice: HTTPServiceService) { }

  ngOnInit() {
  }

  onData(name: string, imageUrl: string, amount: number, description: string) {

    this.shoppingList = new AddShoppingListController(name, imageUrl, amount, description);

    console.log(this.shoppingList)

    this.httpservice.onPostShoppingList(this.shoppingList)
      .subscribe(
      data => console.log(data)
      );
  }

}
