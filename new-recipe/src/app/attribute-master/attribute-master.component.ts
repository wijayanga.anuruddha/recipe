import { Component, OnInit } from '@angular/core';
import { AttributeController } from '../attribute-master-controller';
import { HTTPServiceService } from '../httpservice.service';

@Component({
  selector: 'app-attribute-master',
  templateUrl: './attribute-master.component.html'
})
export class AttributeMasterComponent implements OnInit {

  attribute: AttributeController;

  items: any[] = [];

  a: string;

  constructor(private httpservice: HTTPServiceService) { }

  ngOnInit() {
  }

  onDataMasterAttribute(name: string, description: string, format: string, category: string) {

    this.attribute = new AttributeController(name, description, format, category);

    this.httpservice.onPostAttribute(this.attribute)
      .subscribe(
      data => console.log(data)
      );

  }

  loadAttribute() {
    this.httpservice.onGetAttribute()
      .subscribe(
      data => {
        const myArray = [];
        for (let key in data) {
          myArray.push(data[key]);
        }
        this.items = myArray;
        this.a = this.items[0].category;
        console.log(this.a);
      }
      );

  }

}
