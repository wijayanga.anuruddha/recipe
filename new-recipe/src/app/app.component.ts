import { Component } from '@angular/core';
import { AddService } from './add.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  providers: [AddService]
})

export class AppComponent {
  title = 'app works!';

  constructor(private addService: AddService) { }

  onLog(value: string) {
    this.addService.addData(value);
  }
}
