import { Component, OnInit } from '@angular/core';
import { HTTPServiceService } from '../httpservice.service';

@Component({
  selector: 'app-shopping-list-load',
  templateUrl: './shopping-list-load.component.html'
})
export class ShoppingListLoadComponent{

  items: any[] =[];

  constructor(private httpservice: HTTPServiceService) { }

  onDataLoad() {
    console.log("load data");
    this.httpservice.onGetData()
      .subscribe(
        data =>{
          const myArray =[];
          for(let key in data){
            myArray.push(data[key]);
          }
          this.items= myArray;
        }
      );
  }

}
