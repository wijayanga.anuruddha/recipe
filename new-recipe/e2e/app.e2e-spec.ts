import { NewRecipePage } from './app.po';

describe('new-recipe App', () => {
  let page: NewRecipePage;

  beforeEach(() => {
    page = new NewRecipePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
